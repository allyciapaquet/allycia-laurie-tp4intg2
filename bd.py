import mysql.connector 

def obtenir_connexion():
    connexion = mysql.connector.connect(
        user="root",
        password="",
        host="127.0.0.1",
        port=3306,
        database="tp4"
    )
    return connexion
