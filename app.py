from flask import Flask
from bd import obtenir_connexion
import json
from flask_cors import CORS, cross_origin

app = Flask(__name__) 
CORS(app)

@app.route('/')
def accueil():
    return 'Bienvenue'

@cross_origin()
@app.route('./tp4/maliste')
def maListe():
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute(
        "SELECT * FROM `maliste` ")
    resultat = curseur.fetchall()
    liste = []
    for rangee in resultat:
        liste.append({"id": rangee[0], "bonbons": rangee[1], "prix": rangee[2]})
    return json.dumps(liste)


@app.route('./tp4/maliste/<id>')
def maListe(id):
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute(
        "SELECT * FROM `maliste` WHERE id = %s", (id,))
    resultat = curseur.fetchall()
    liste = []
    for rangee in resultat:
        liste.append({"id": rangee[0], "bonbons": rangee[1], "prix": rangee[2]})
    return json.dumps(liste)


if __name__ == "__main__":
    app.run()